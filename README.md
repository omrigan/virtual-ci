# Virtual CI
Continious integration pipeline is able to run IGT to test virtual kernel modules (vkms, vgem).

The pipeline (.gitlab-ci.yml) consists of 4 stages:
  - qemu-base - prepares a QEMU Alpine image with docker inside and puts into docker image (called `qemu-base`).
  - kernel-base - prepares a docker image (called `kernel-base`) with all stuff needed to compile a kernel.
  - kernel - runs `kernel-base`, pulls fresh version of a kernel.
  - test - runs `qemu-base`, takes the kernel from previous step, runs tests.

`qemu-base-old` is an obsolete directory with first attempt to make a debian-based image instead of alpine one.

## ToDo
 - Docker inside Alpine reports as started, but can't run containers. 
 - Figure out a way to cache kernel build. One way could be rebuild kernel-base every ~week, so that build cache would be in a base docker image.




