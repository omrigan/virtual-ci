#!/bin/sh
set -e

#qemu-img create -f raw final.img 2G
dd if=/dev/zero of=final.img bs=4k count=500000

alpine-make-vm-image/alpine-make-vm-image -p "docker e2fsprogs openssh" \
	final.img setup-image.sh

docker build .
