pwd
echo "rc_verbose=yes" > etc/conf.d/local


SCRIPT=$(cat <<- END
	echo Hello;
	echo $(whoami);
	rc-status;
	DOCKERRUN=$(cat /proc/cmdline | tr '  ' ' ' | tr ' ' '\n' | grep "^dockerrun" | tr '=' '\n' | tail -1);
	echo $DOCKERRUN;
	ls -l /var/run/docker.sock;
	service docker status;
	docker run hello-world;
END
)

echo $SCRIPT > etc/local.d/dockerrun.start
chmod +x etc/local.d/dockerrun.start
chroot . rc-update add local default
chroot . rc-update add docker boot 
chroot . rc-update add sshd 
