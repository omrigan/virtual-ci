#!/bin/sh
qemu-system-x86_64 -smp 1 -m 2048 \
	-drive file=/qemu/final.img,format=raw \
	-vnc :0 \
	-serial stdio \
	-kernel `pwd`/kernel/vmlinuz-linux \
	-netdev user,hostname=test,hostfwd=tcp::22-:22,hostfwd=udp::22-:22,id=net\
	-device virtio-rng-pci \
	-device virtio-net-pci,netdev=net \
	-append "root=/dev/sda modules=ext4 console=ttyS0 random.trust_cpu=1 dockerrun=hidocker"
#qemu-system-x86_64 -enable-kvm -smp 1 -m 2048 -vnc :0  \
          #-serial stdio \
          #-hda /tmp/final.img \
          #-kernel /tmp/kernel/vmlinuz-linux \
          #-append "root=/dev/sda modules=ext4 console=ttyS0 random.trust_cpu=1"
