#!/bin/bash
set -e

BASEDIR=${1:-`pwd`/drm-tip}
TARGET=`pwd`/kernel
mkdir -p $TARGET/

cd $BASEDIR
make -j $(nproc)

cp -v $BASEDIR/arch/x86_64/boot/bzImage $TARGET/vmlinuz-linux
VERSION=$(file -bL $TARGET/vmlinuz-linux | grep -o 'version [^ ]*' | cut -d ' ' -f 2)

mkdir -p $TARGET/modulesdir/lib/modules
umount $TARGET/modules.img || echo "Not moutned"

rm $TARGET/modules.img || echo "Not present"
dd if=/dev/zero of=$TARGET/modules.img bs=4k count=100000
mkfs.ext4 $TARGET/modules.img
mount -o loop $TARGET/modules.img $TARGET/modulesdir/lib/modules && echo "Mounting"
make modules_install -C $BASEDIR INSTALL_MOD_PATH=$TARGET/modulesdir

#mkinitcpio -r $TARGET/modulesdir -k $VERSION -g $TARGET/initrd
umount $TARGET/modulesdir/lib/modules || echo "Error" 


