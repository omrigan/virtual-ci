#!/bin/sh
#
# Copyright © 2019 Intel Corporation
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

DOCKERFILE=$1
NAME=$2
TAG=${3:-${CI_COMMIT_REF_NAME:-latest}}

TAG=$(echo $TAG | tr / - )
if [ $TAG = "master" ]; then
	TAG=latest
fi

CHECKSUM=$(sha1sum $DOCKERFILE | cut -d ' ' -f1)
FULLNAME=$CI_REGISTRY/$CI_PROJECT_PATH/$NAME:$TAG
CHECKNAME=$CI_REGISTRY/$CI_PROJECT_PATH/$NAME:$CHECKSUM

docker pull $CHECKNAME
IMAGE_PRESENT=$?

set -e
if [ $IMAGE_PRESENT -eq 0 ] && [ ${FORCE_REBUILD:-0} -eq 0 ] ; then
	echo "Skipping $NAME:$TAG, already built"
	docker tag $CHECKNAME $FULLNAME
	docker tag $CHECKNAME $NAME
else
	echo "Building $NAME:$TAG"
	docker build -t $CHECKNAME -t $FULLNAME -t $NAME -f $DOCKERFILE .
	docker push $CHECKNAME
fi
docker push $FULLNAME
