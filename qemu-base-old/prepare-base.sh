#!/bin/bash
set -e

genisoimage  -output seed.iso -volid cidata -joliet -rock user-data meta-data

## create a new qcow image to boot, backed by your original image
qemu-img create -f qcow2 -b debian-10-openstack-amd64.qcow2 final.qcow2 5G

qemu-system-x86_64 -enable-kvm -smp 1 -m 2048 -vnc :0 -serial stdio \
-netdev user,hostname=7b721ae7fa4c,hostfwd=tcp::22-:22,hostfwd=udp::22-:22,id=net\
-device virtio-rng-pci \
-device virtio-net-pci,netdev=net \
-drive file=seed.iso,format=raw,if=virtio \
-hda final.qcow2 
