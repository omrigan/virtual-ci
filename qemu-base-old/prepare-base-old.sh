#!/bin/bash
set -e

genisoimage  -output seed.iso -volid cidata -joliet -rock user-data meta-data

## create a new qcow image to boot, backed by your original image
qemu-img create -f qcow2 -b debian-10-openstack-amd64.qcow2 temp.qcow2 5G
cp temp.qcow2 final.qcow2
#virt-resize --expand /dev/sda1 temp.qcow2 final.qcow2

docker run -it --rm \
	--device /dev/kvm \
	--device /dev/hwrng:/dev/hwrng:rw \
	--dns 8.8.8.8 \
	--net soxy_network \
	--name qemu-container \
	--privileged \
	-p 2222:22 \
	-v `pwd`/debian-10-openstack-amd64.qcow2:/tmp/debian-10-openstack-amd64.qcow2:ro \
	-v `pwd`/final.qcow2:/tmp/final.qcow2\
	-v `pwd`/seed.iso:/tmp/seed.iso \
	-v `pwd`/kernel:/tmp/kernel \
	tianon/qemu qemu-system-x86_64 -enable-kvm -smp 1 -m 2048 -vnc :0 -serial stdio \
	-netdev user,hostname=7b721ae7fa4c,hostfwd=tcp::22-:22,hostfwd=udp::22-:22,id=net\
	-device virtio-rng-pci \
	-device virtio-net-pci,netdev=net \
	-drive file=/tmp/seed.iso,format=raw,if=virtio \
	-hda /tmp/final.qcow2 
