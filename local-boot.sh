#!/bin/bash
set -e
# Mount read-only

#qemu-system-x86_64 -enable-kvm -smp 1 -m 2048  \
  #-drive format=raw,file=qemu-base/final.img \
  #-kernel compile-kernel/kernel/vmlinuz-linux \
  #-serial stdio \
  #-append "root=/dev/sda console=ttyS0 random.trust_cpu=1" 
	  #-hda /tmp/final.img \

docker run  \
	-it --rm \
	--device /dev/kvm \
	--cap-add SYS_ADMIN \
	--dns 8.8.8.8 \
	--net soxy_network \
	--name qemu-container \
	--privileged \
	-p 2222:22 \
	-v `pwd`/compile-kernel/kernel:/tmp/kernel \
	qemu-base ./boot.sh

	#-v `pwd`/qemu-base/final.img:/tmp/final.img \
	#tianon/qemu qemu-system-x86_64 -enable-kvm -smp 1 -m 2048 \
	  #-drive file=/tmp/final.img,format=raw \
          #-vnc :0 \
	  #-serial stdio \
	  #-kernel /tmp/kernel/vmlinuz-linux \
	  #-netdev user,hostname=test,hostfwd=tcp::22-:22,hostfwd=udp::22-:22,id=net\
          #-device virtio-rng-pci \
          #-device virtio-net-pci,netdev=net \
	  #-append "root=/dev/sda modules=ext4 console=ttyS0 random.trust_cpu=1" 

